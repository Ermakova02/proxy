package ru.nsu.ccfit.ermakova;

import java.io.IOException;

public class SocksProxy {
    public static void main(String [] args) {
        if (args.length != 1) {
            System.out.println("Usage: java SocksProxy [port]");
            return;
        }
        int port = 0;
        try {
            port = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return;
        }
        ProxyServer srv = new ProxyServer(port);
        try {
            srv.start();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
}
