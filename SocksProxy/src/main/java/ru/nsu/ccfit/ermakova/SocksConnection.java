package ru.nsu.ccfit.ermakova;

public class SocksConnection {
    private SocksConnectStage stage;
    private String domain;
    private int port;

    SocksConnection() {
        this(SocksConnectStage.WAITING_FOR_AUTH_METHODS);
    }

    SocksConnection(SocksConnectStage stage) {
        this.stage = stage;
        domain = null;
        port = -1;
    }
    public void setStage(SocksConnectStage stage) { this.stage = stage; }
    public void setDomain(String domain) { this.domain = new String(domain); }
    public void setPort(int port) { this.port = port; }
    public SocksConnectStage getStage() { return stage; }
    public String getDomain() { return domain; }
    public int getPort() { return port; }

    public String toString() {
        return new String(domain + ":" + Integer.toString(port) + ", " + stage.toString());
    }
}
